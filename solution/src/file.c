//
// Created by Александр Щапов on 01/11/2022.
//

#include "../include/file.h"
#include <stdio.h>

int file_open(FILE** f1, char* file1, char* mode)
{
    *f1 = fopen(file1, mode);
    if (*f1 == NULL)

    {
        fprintf(stderr,"! File '%s' was NOT opened!\n",file1);
        return 1;
    }
    else
    {
        printf("File '%s' was opened!\n",file1);
        return 0;
    }
}

int file_close(FILE** f1, char* file1)
{
    if (fclose(*f1))
    {
        fprintf(stderr,"! File '%s' was NOT closed!\n", file1);
        return 1;
    }
    else
    {
        printf("File '%s' was closed!\n", file1);
        return 0;
    }
}

int file_read(struct bmp_header* head, struct image* image, uint64_t* padding, FILE* f1, char* file1)
        {
    h_read(head, sizeof(struct bmp_header), 1, f1, file1);
    image_init(image, head->biWidth, head->biHeight);
    *padding = (4 - (int64_t)head->biWidth * sizeof(struct pixel) % 4) % 4;
    d_read(head, image,sizeof (struct pixel),head->biWidth, f1, file1, padding);
    return 0;
        }

int file_write(struct bmp_header* head, struct image* image, FILE* f1, char* file1, char nuly[3], uint64_t* padding)
        {
    h_write(head, image, f1, file1);
    d_write(head, image, f1, file1, nuly, padding);
    return 0;
        }
