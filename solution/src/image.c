//
// Created by Александр Щапов on 01/11/2022.
//
#include "../include/image.h"
#include <stdio.h>
#include <stdlib.h>


void image_init(struct image* image, int64_t width, int64_t height) {
    image->data = malloc(height * width * sizeof(struct pixel));
    image->width = width;
    image->height = height;
}

int d_read(struct bmp_header* head, struct image* image, size_t size, size_t numitems, FILE* f1, char* file1, uint64_t const* padding) {
    int32_t c = 0;
    for (uint32_t i = 0; i < head->biHeight; i++)
    {
        if (fread(image->data + i*head->biWidth,size,numitems,f1) == 0)
        {
            fprintf(stderr, "! String №%d was NOT read!\n", i);
            c++;
        }
        if (fseek(f1,(long)*padding,SEEK_CUR) != 0)
        {
            printf("! String №%d was NOT seeked!\n", i);
            c++;
        }
    }
    if (c != 0)
    {
        fprintf(stderr,"! File' data '%s' was NOT read!\n", file1);
        return 1;
    }
    else
    {
        printf("File's data '%s' was read!\n", file1);
        return 0;
    }
}

int d_write(struct bmp_header* head, struct image* image, FILE* f1, char* file1, char nuly[3], uint64_t* padding) {
    *padding = (4 - head->biWidth * sizeof(struct pixel) % 4) % 4;
    int32_t c = 0;
    for (uint32_t i = 0; i < head->biHeight; i++)
    {
        if (fwrite(image->data + i * head->biWidth, sizeof(struct pixel), head->biWidth, f1) != image->width || fwrite(nuly, 1, *padding, f1) != *padding) {
            fprintf(stderr, "! String №%d was not successfully written!\n", i);
            c++;
        }
    }
    if (c != 0) {
        fprintf(stderr, "! Data file '%s' was NOT written!\n", file1);
        return 1;
    } else {
        printf("Data file '%s' was written!\n", file1);
        return 0;

    }

}
struct image i_rotate(struct image* old_image, struct image* new_image) {
    for (uint32_t i = 0; i < old_image->height; i++) {
        for (uint32_t j = 0; j < old_image->width; j++) {
            new_image->data[j * old_image->height + (old_image->height - 1 - i)] = old_image->data[i * old_image->width + j];
        }
    }
    if (new_image->data == NULL) {
        fprintf(stderr, "! Image was NOT rotated!\n");
    } else {
        printf("Image was rotated!\n");
    }

    return *new_image;
}
