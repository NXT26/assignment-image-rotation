
#include "bmp.h"
#include "file.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {

    (void) argc;
    (void) argv;

    if (argc != 3) {
        fprintf(stderr, "%s", "You have to enter 2 program arguments!");
        return 1;
    }


    char *f_in = argv[1];
    char *f_out = argv[2];

    FILE* file_input;
    FILE* file_output;

    struct bmp_header head;
    struct image old_image;
    struct image new_image;


    uint64_t padding;
    char nuly[3] = {0,0,0};

    file_open(&file_input, f_in, "rb");
    file_open(&file_output, f_out, "wb");
    file_read(&head, &old_image, &padding, file_input, f_in);

    image_init(&new_image, (int64_t)old_image.height, (int64_t)old_image.width);
    new_image = i_rotate(&old_image, &new_image);

    file_write(&head, &new_image, file_output, f_out, nuly, &padding);

    file_close(&file_input, f_in);
    file_close(&file_output,f_out);

    free(new_image.data);
    free(old_image.data);
    return 0;
}
