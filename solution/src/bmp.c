//
// Created by Александр Щапов on 01/11/2022.
//

#include "../include/bmp.h"
#include <stdio.h>

int h_read(struct bmp_header* head, size_t size, size_t numitems, FILE* f1, char* file1)
        {
    if (fread(head, size, numitems, f1) != 1) {
        fprintf(stderr,"! Header '%s' was NOT read!\n", file1);
        return 1;
    }
    else
    {
        printf("Header '%s' was read!\n", file1);
        return 0;
    }
}

struct bmp_header h_create(struct image* image)
        {
    struct bmp_header head = {
            .bfType = 19778,
            .biSize = 40,
            .biPlanes = 1,
            .bfileSize = image->height * image->width * sizeof(struct pixel) + image->height * ((4 - ((3 * image->width) % 4)) % 4),
            .biWidth = image->width,
            .biHeight = image->height,
            .biBitCount = 24,
            .bOffBits = sizeof(struct bmp_header) };
    return head;
}

int h_write(struct bmp_header* head, struct image* image, FILE* f1, char* file1)
        {
    *head = h_create(image);
    if (fwrite(head, sizeof(struct bmp_header), 1, f1) != 1)
    {
        fprintf(stderr, "! Header file '%s' was NOT written!\n", file1);
        return 1;
    }
    else
    {
        printf("Header file '%s' was written!\n", file1);
        return 0;
    }

        }
