//
// Created by Александр Щапов on 01/11/2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_H

#include "../include/image.h"
#include <stdint.h>
#include <stdio.h>



struct bmp_header;

struct image;

int file_open(FILE** f1, char* file1, char* mode);

int file_close(FILE** f1, char* file1);

int file_read(struct bmp_header* head, struct image* image, uint64_t* padding, FILE* f1, char* file1);

int file_write(struct bmp_header* head, struct image* image, FILE* f1, char* file1, char nuly[3], uint64_t* padding);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_H
