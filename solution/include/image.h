//
// Created by Александр Щапов on 01/11/2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include "../include/bmp.h"
#include "../include/file.h"
#include "inttypes.h"
#include <stdio.h>


struct pixel{uint8_t b, g, r;};

struct image{
    uint64_t width, height;
    struct pixel* data;
    };

void image_init(struct image* image, int64_t width, int64_t height);

int d_read(struct bmp_header* head, struct image* image, size_t size, size_t numitems, FILE* f1, char* file1, uint64_t const* padding);

int d_write(struct bmp_header* head, struct image* image, FILE* f1, char* file1, char nuly[3], uint64_t* padding);

struct image i_rotate(struct image* old_image, struct image* new_image);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
