//
// Created by Александр Щапов on 01/11/2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H
#include "../include/image.h"
#include  <stdint.h>
#include <stdio.h>


#pragma pack(push, 1)

struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

#pragma pack(pop)

struct image;

int h_read(struct bmp_header* head, size_t size, size_t numitems, FILE* f1, char* file1);

struct bmp_header h_create(struct image* image);

int h_write(struct bmp_header* head, struct image* image, FILE* f1, char* file1);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
